package labo3;
import io.Console;

public class SimulationEmprunt {
    public static void main(String[] args) {
        // Demander les variables à l'utilisateur
        System.out.print("Capital ? ");
        double capital = Console.lireDouble();

        System.out.print("Taux annuel en % ? ");
        double tauxAnnuelPer = Console.lireDouble();

        System.out.print("Durée 1 en années ? ");
        int duree1 = Console.lireInt();

        System.out.print("Durée 2 en années ? ");
        int duree2 = Console.lireInt();

        // Afficher les valeurs communes
        double tauxMensuel = Emprunt.calculerTauxMensuel(tauxAnnuelPer) * 100;
        System.out.printf("\nCapital à emprunter = %.2f EUR\n", capital);
        System.out.printf("Taux d'intérêt mensuel = %.2f%%\n\n", tauxMensuel);

        // Pour chaque durée, calculer le remboursement mensuel, le remboursement total et l'interet pret
        afficherSimulationEmprunt(capital, duree1, tauxAnnuelPer);
        afficherSimulationEmprunt(capital, duree2, tauxAnnuelPer);
    }
    
    private static void afficherSimulationEmprunt(double capital, int periodeAnnees, double tauxAnnuelPourcentage) {
        double remboursementMensuel = Emprunt.calculerMensualite(capital, periodeAnnees, tauxAnnuelPourcentage);
        double total = remboursementMensuel * periodeAnnees * 12;
        System.out.printf("Sur %d ans :\n", periodeAnnees);
        System.out.printf("Nombre de paiements = %d\n", periodeAnnees * 12);
        System.out.printf("Remboursement mensuel = %.2f EUR\n", remboursementMensuel);
        System.out.printf("Total remboursé = %.2f EUR\n", total);
        System.out.printf("Intérêts prêt = %.2f EUR\n\n", total - capital);
    }
}
