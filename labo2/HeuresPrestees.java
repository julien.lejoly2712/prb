package labo2;
import io.Console;

public class HeuresPrestees {
    public static void main (String[] args) {
        //récupérer les heures de prestation
            //lundi
                // Variables
                int string1Lenght, string1HoursInt, string1MinutesInt;
                String string1, string1Hours, string1Minutes;

                // Récupération des heures de prestation
                System.out.print("Prestations du lundi ? ");
                string1 = Console.lireString();

                // Récupérer l'heure et les minutes en int
                string1Lenght = string1.length();
                string1Hours = string1.substring(0, string1Lenght - 3);                 // On fait string1Lenght-3 pour avoir une position relative à la fin qui elle ne change pas (contrairement à celle du début qui peut avoir 2 positions 7 ou 15)
                string1Minutes = string1.substring(string1Lenght - 2, string1Lenght);
                string1HoursInt = Integer.parseInt(string1Hours);
                string1MinutesInt = Integer.parseInt(string1Minutes);

            //mardi
                // Variables
                int string2HoursInt, string2MinutesInt;

                // Récupération des heures de prestation
                System.out.print("Prestations du mardi ? ");
                string1 = Console.lireString();

                // Récupérer l'heure et les minutes en int
                string1Lenght = string1.length();
                string1Hours = string1.substring(0, string1Lenght - 3);                 // On fait string1Lenght-3 pour avoir une position relative à la fin qui elle ne change pas (contrairement à celle du début qui peut avoir 2 positions 7 ou 15)
                string1Minutes = string1.substring(string1Lenght - 2, string1Lenght);
                string2HoursInt = Integer.parseInt(string1Hours);
                string2MinutesInt = Integer.parseInt(string1Minutes);

            //mercredi
                // Variables
                int string3HoursInt, string3MinutesInt;

                // Récupération des heures de prestation
                System.out.print("Prestations du mercredi ? ");
                string1 = Console.lireString();

                // Récupérer l'heure et les minutes en int
                string1Lenght = string1.length();
                string1Hours = string1.substring(0, string1Lenght - 3);                 // On fait string1Lenght-3 pour avoir une position relative à la fin qui elle ne change pas (contrairement à celle du début qui peut avoir 2 positions 7 ou 15)
                string1Minutes = string1.substring(string1Lenght - 2, string1Lenght);
                string3HoursInt = Integer.parseInt(string1Hours);
                string3MinutesInt = Integer.parseInt(string1Minutes);

            //jeudi
                // Variables
                int string4HoursInt, string4MinutesInt;

                // Récupération des heures de prestation
                System.out.print("Prestations du jeudi ? ");
                string1 = Console.lireString();

                // Récupérer l'heure et les minutes en int
                string1Lenght = string1.length();
                string1Hours = string1.substring(0, string1Lenght - 3);                 // On fait string1Lenght-3 pour avoir une position relative à la fin qui elle ne change pas (contrairement à celle du début qui peut avoir 2 positions 7 ou 15)
                string1Minutes = string1.substring(string1Lenght - 2, string1Lenght);
                string4HoursInt = Integer.parseInt(string1Hours);
                string4MinutesInt = Integer.parseInt(string1Minutes);

            //vendredi
                // Variables
                int string5HoursInt, string5MinutesInt;

                // Récupération des heures de prestation
                System.out.print("Prestations du vendredi ? ");
                string1 = Console.lireString();

                // Récupérer l'heure et les minutes en int
                string1Lenght = string1.length();
                string1Hours = string1.substring(0, string1Lenght - 3);                 // On fait string1Lenght-3 pour avoir une position relative à la fin qui elle ne change pas (contrairement à celle du début qui peut avoir 2 positions 7 ou 15)
                string1Minutes = string1.substring(string1Lenght - 2, string1Lenght);
                string5HoursInt = Integer.parseInt(string1Hours);
                string5MinutesInt = Integer.parseInt(string1Minutes);

        //Additionner toutes les heures
            //Convertir heures en minutes
            int hEnM = (string1HoursInt + string2HoursInt + string3HoursInt + string4HoursInt + string5HoursInt) * 60;

            //Minute total
            int totalTime1 = hEnM + (string1MinutesInt + string2MinutesInt + string3MinutesInt + string4MinutesInt + string5MinutesInt);

            //reconvertir en hh:mm
            int totalH = totalTime1 / 60;
            totalTime1 %= 60;

            int totalM = totalTime1;

        //calculer le tarif
            //tarif à l'heure = 18.75€ / h
            double tarifParHeure = 18.75;
            double tarifHeure, tarifMinute, tarifTotal;

            tarifHeure = totalH * tarifParHeure;
            tarifMinute = totalM * (tarifParHeure / 60);
            tarifTotal = tarifMinute + tarifHeure;

        //Afficher les informations
            //durée hebdomadaire
            System.out.printf("Durée hebdomadaire : %d:%d\n", totalH, totalM);

            //montant à facturer
            System.out.printf("Montant à facturer : %.2f €\n", tarifTotal);
    }
}
