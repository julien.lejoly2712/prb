package labo2;
import io.Console;

public class Arrondis {
    public static void main(String[] args) {
        while (true) {
            double valeur = Console.lireDouble();
            double vceil = Math.ceil(valeur);
            double vfloor = Math.floor(valeur);
            double vrint = Math.floor(valeur);
            double vround = Math.floor(valeur);
            System.out.printf("valeur: %f | ceil: %f | floor: %f | rint: %f | round: %f\n", valeur, vceil, vfloor, vrint, vround);
        }
    }
}
