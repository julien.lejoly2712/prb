package labo2;
import io.Console;

public class Telechargement {
    public static void main(String[] args) {
        System.out.print("Nom du fichier ? ");
        String filename = Console.lireString();

        System.out.print("Taille du fichier en Mo ? ");
        double tailleMo = Console.lireDouble();

        System.out.print("Débit en Mbps ? ");
        double debit = Console.lireDouble();

        double tailleMb = tailleMo * 8;
        double total = tailleMb / debit;

        int heures = (int)(total / 60 / 60);
        total -= heures * 60 * 60;

        int minutes = (int)(total / 60);
        total -= minutes * 60;

        int secondes = (int)total;
        total -= secondes;

        System.out.printf("Durée du téléchargement du fichier '%s' estimée à %dh %dm %dsec\n", filename, heures, minutes, secondes );
    }
}
