package labo5;
import io.Console;

public class Casino {
    public static void main(String[] args) {
        // 1. Déclaration des variables
        // Variables d'acquisition
        int depart;
        int objectif;

        // Variables de traitement
        int argent;
        int reussites = 0;
        int paris = 0;
        int gains = 0;

        // 2. Acquisition 
        System.out.print("Mise de départ ? ");
        depart = Console.lireInt();
        System.out.print("Gains souhaités ? ");
        objectif = Console.lireInt();
        
        // 3. Traitement
        argent = depart;
        while(argent < depart + objectif && argent > 0) {
            if (Math.random() < 0.5) {
                argent += 1;
                reussites += 1;
            } else { 
                argent -= 1;
            }
            paris += 1;
        }

        gains = argent - depart;
        
        // 4. Affichage des résultats
        System.out.printf("Vous avez réussi %d paris sur %d\n", reussites, paris);
        System.out.printf("Vous repartez avec %d EUR de plus\n", gains);
    }
}
