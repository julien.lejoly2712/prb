package labo4;

public class Droites {
    public static void main(String[] args) {
        // Constantes de messages
        final String message_parallele = "Les droites sont parallèles";
        final String message_perpendiculaire = "Les droites sont perpendiculaires";
        final String message_secantes = "Les droites sont sécantes";
        final String message_invalide = "Coordonnées incorrectes !";

        // Déclaration des variables d'acquisition, transfert et affichage
        String coordA, coordB, coordC, coordD;
        double xA, xB, yA, yB, xC, yC, xD, yD;
        double coefficient_AB, coefficient_CD;

        // Acquisition des coordonnées
        System.out.print("Coordonnée A (x y) ? ");
        coordA = io.Console.lireString();

        System.out.print("Coordonnée B (x y) ? ");
        coordB = io.Console.lireString();

        System.out.print("Coordonnée C (x y) ? ");
        coordC = io.Console.lireString();

        System.out.print("Coordonnée D (x y) ? ");
        coordD = io.Console.lireString();

        String regex = "^[-,+]?[0-9]+ [-,+]?[0-9]+$";
        if (coordA.matches(regex) && coordB.matches(regex) && coordC.matches(regex) && coordD.matches(regex)) {
            /* Calcul */
            // Transformation des strings en double
            xA = lireCoordX(coordA);
            yA = lireCoordY(coordA);
            xB = lireCoordX(coordB);
            yB = lireCoordY(coordB);

            xC = lireCoordX(coordC);
            yC = lireCoordY(coordC);
            xD = lireCoordX(coordD);
            yD = lireCoordY(coordD);

            // Calcul des coefficients
            coefficient_AB = coefficientDirecteur(xA, yA, xB, yB);
            coefficient_CD = coefficientDirecteur(xC, yC, xD, yD);

            /* Affichage des résultats */
            if (coordA.equals(coordB) || coordC.equals(coordD)){ 
                System.out.println(message_invalide);
            } else {
                System.out.printf("Coefficient directeur de la droite AB : %.3f\n", coefficient_AB);
                System.out.printf("Coefficient directeur de la droite CD : %.3f\n", coefficient_CD);
                
                if (coefficient_AB == coefficient_CD) {
                    System.out.println(message_parallele);
                } else if ((coefficient_AB * coefficient_CD) == -1) {
                    System.out.println(message_perpendiculaire);
                } else {
                    System.out.println(message_secantes);
                }
            }

            
        } else {
            System.out.println(message_invalide);
        }
    }

    public static double lireCoordX(String coordPoint) {
        return Double.parseDouble(coordPoint.split(" ")[0]);
    }

    public static double lireCoordY(String coordPoint) {
        return Double.parseDouble(coordPoint.split(" ")[1]);
    }

    public static double coefficientDirecteur(double xA, double yA, double xB, double yB) {
        if (xA == xB) {
            if (yA == yB) {
                return Double.NaN;
            } else {
                return Double.POSITIVE_INFINITY;
            }
        } else {
            return (yB - yA) / (xB-xA);
        }
    }
}
